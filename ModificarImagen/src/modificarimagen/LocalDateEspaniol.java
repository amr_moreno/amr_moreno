package modificarimagen;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1DAM
 */
public class LocalDateEspaniol {
    private LocalDate fecha;
    
     public LocalDateEspaniol (Long ms){
   Date aux=new Date(ms);
   this.fecha=aux.toLocalDate();
    }
     public LocalDateEspaniol (int anio, int mes, int dia){
         this.fecha=LocalDate.of(dia, mes,anio);
     }
    
    public LocalDateEspaniol (LocalDate orig){
   fecha=orig;
    }
    public  LocalDate getFecha(){
        return fecha; 
    }
     public  void setFecha(LocalDate ld){
       this.fecha=ld;
    }
    
    
    public String imprimir(){
        return this.fecha.getDayOfWeek()+"/"+this.fecha.getDayOfMonth()+"/"+this.fecha.getDayOfYear();
    }
    
}
