/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modificarimagen;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author 1DAM
 */
public class Fechas {
    public static void trabajarConFechas(){
        //usamos primero Date de sql, que se corresponde con una columna de tipo daste.
        //El contructor recibe los milisegundos desde epoc 
        Date fecha=new Date(1);
        System.out.println(fecha);
        //Vamos a comberit run String a una fecha
        Date hoy=Date.valueOf("2019-03-19");
        System.out.println(hoy);
         System.out.println("\n");
        //La voy a comvertir a LocalDate
        LocalDate hoyld= hoy.toLocalDate();
        //vamos a imprimirla  de distintas formas 
        System.out.println("hoy LocalDate: "+ hoyld);

        System.out.println("hoy LocalDate: "+ hoyld.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println("hoy LocalDate: "+ hoyld.format(DateTimeFormatter.ISO_DATE));
        System.out.println("\n");
        //imprimir año, mes , semana, etc
        System.out.println("Dia del año: "+ hoyld.getDayOfYear());
        System.out.println("Dia del mes: "+ hoyld.getDayOfMonth());
             System.out.println("Dia de la semana: "+ hoyld.getDayOfWeek());
             System.out.println("Año "+ hoyld.getYear());
             LocalDate ayer=hoyld.minus(Period.ofDays(1));
             System.out.println(ayer);
             System.out.println("\n");
             //vamos a sacara dia intermedia entre dos fechas
             LocalDate fecha1=Date.valueOf("1997-08-19").toLocalDate();
              LocalDate fecha2=Date.valueOf("1986-07-23").toLocalDate();
             long diasCumpleAndres=fecha1.toEpochDay();
              long diasCumpleMiguel=fecha2.toEpochDay();
              System.out.println("Dias cumple Andres: "+diasCumpleAndres );
              System.out.println("Dias cumple Miguel: "+diasCumpleMiguel ); 
              long intermedia=(diasCumpleAndres+diasCumpleMiguel)/2;
              
              LocalDate FechaIntermedia=(new Date(intermedia*86400000L)).toLocalDate();
              System.out.println("El dia intermedio= "+ FechaIntermedia);
              System.out.println("\n");
              //Metodo 2 para sacar la media
              long mitadDif=(diasCumpleAndres-diasCumpleMiguel)/2;
              LocalDate FechaIntermedia2=fecha2.plusDays(mitadDif);
               System.out.println("El dia intermedio= "+ FechaIntermedia2);
               System.out.println("\n");
               //Vamos a emepezar a porbar TimeStamp
               //Esta clase se corresponde con el timpo Timestamp de SQL
               //Representa fecha con hora, min , segundtos , ect.
               System.out.println("\n\n\n Trabajando TimeStamp");
               Timestamp ts=new Timestamp(123);
               System.out.println(ts);
               
               //En realidad Timestamp se nos queda como clase  para sacar un localDAteTime
               System.out.println("\n\n Trabajamos con LocalDateTime");
               LocalDateTime idt=ts.toLocalDateTime();
               
               //vamos ah imprimir de distintas formas 
               System.out.println(idt.format(DateTimeFormatter.ISO_DATE)); 
                   System.out.println(idt);
                   System.out.println(idt.getMonth());
                   System.out.println(idt.getMonthValue());
                   System.out.println(" momento actual: "+LocalDateTime.now());
                   
                   LocalDateTime dt2= LocalDateTime.of(-500, 
                           Month.DECEMBER, 1, 8, 30, 00);
                   System.out.println(dt2);
                   
                   System.out.println("Parseada desde String "+ LocalDateTime.parse("2019-03-28T11:30:00"));
                   
                   System.out.println("Usando mi propia clase Fecha ");
                   LocalDateEspaniol ide= new LocalDateEspaniol(6546565464L);
                   
                  System.out.println(ide.imprimir());
               LocalDateEspaniol idee= new LocalDateEspaniol(20,3,2019);
                System.out.println(idee.imprimir());
               
    }
}
