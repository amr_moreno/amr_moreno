/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioclase;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author 1DAM
 */
public class DiaExamen {
     public static void DiaParaELExamen()  {
       LocalDateTime fActual=LocalDateTime.now();
            LocalDateTime fExamen=LocalDateTime.of(2019,03,28,11,45,00);
            LocalDateTime tempDateTime=LocalDateTime.from(fActual);

            long dias=tempDateTime.until(fExamen,ChronoUnit.DAYS);
            tempDateTime=tempDateTime.plusDays(dias);

            long horas=tempDateTime.until(fExamen,ChronoUnit.HOURS);
            tempDateTime=tempDateTime.plusHours(horas);

            long minutos=tempDateTime.until(fExamen,ChronoUnit.MINUTES);
            tempDateTime=tempDateTime.plusMinutes(minutos);

            long segundos=tempDateTime.until(fExamen,ChronoUnit.SECONDS);

            System.out.println("Quedan "+
                    dias + " Dias " +
                    horas + " Horas " +
                    minutos + " Minutos " +
                    segundos + " Segundos"
            );    
 
        
}}
     
