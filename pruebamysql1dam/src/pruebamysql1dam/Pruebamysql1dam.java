/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebamysql1dam;

import com.mysql.cj.xdevapi.Result;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1DAM
 */
public class Pruebamysql1dam {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Connection conexion;
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/PrimeraBase", "root", "");
            Statement smt = conexion.createStatement();
            //ExecuteUpdate
            //sive para modificar base s de datos 
            ResultSet resultado = smt.executeQuery("select * from employeess "
                    + "where id='12345678C'");

            if (!resultado.next()) {
                smt.executeUpdate("insert into employeess values("
                        + "'12345678c','Alfredo tocapelotas ',"
                        + " '1998-04-09')");

            } else {

                String id = resultado.getString("id");
                String name = resultado.getString("name");
                Date fecha = resultado.getDate("birthDate");
                System.out.println(id + " " + name + " " + fecha);
            }

            smt.executeUpdate("create table if not exists employeess("
                    + "id varchar(9) primary key,"
                    + "name varchar (100),"
                    + "birthDate date"
                    + ");");

            smt.close();
            conexion.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

}
