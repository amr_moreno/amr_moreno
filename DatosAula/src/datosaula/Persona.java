/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datosaula;

import excepciones.generoEquibocado;
import excepciones.alturaInadecuadaException;
import excepciones.nombreMayorException;
import excepciones.DniInvalidoException;
import excepciones.hasNacidoMAñanaException;
import java.time.LocalDate;

/**
 *
 * @author 1DAM
 */
public class Persona {

    private String nombre;
    private String dni;
    private LocalDate nacimiento;
    private Float altura;
    private String genero;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws nombreMayorException {
        if (nombre.length() > 50) {
            throw new nombreMayorException("El nombre es muy largo");
        }
        this.nombre = nombre;
    }

    public String getDni() {

        return dni;
    }

    public void setDni(String dni) throws DniInvalidoException {
        if (dni.length() != 9) {
            throw new DniInvalidoException("El DNI ha fallado");
        }
        this.dni = dni;

    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(LocalDate nacimiento) throws hasNacidoMAñanaException {
        if (nacimiento.isAfter(LocalDate.now())) {
            throw new hasNacidoMAñanaException("No puedes nacer en una fecha posterior a hoy");
        }
        this.nacimiento = nacimiento;
    }

    public Persona(String nombre, String dni, LocalDate nacimiento, Float altura, String genero) throws DniInvalidoException, hasNacidoMAñanaException, nombreMayorException, alturaInadecuadaException, generoEquibocado {
        setNombre(nombre);
        setDni(dni);
        setNacimiento(nacimiento);
        setAltura(altura);
        setGenero(genero);
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) throws generoEquibocado {
        
        if (genero.equals("Hombre" )||genero.equals("Mujer")||genero.equals("Otro")||genero.equals("No inidicado")) {
           this.genero=genero;
        }else{
        throw new generoEquibocado("El genero introducido esta equibocado");
}
    }

    public Float getaAltura() {
        return altura;
    }

    public void setAltura(Float altura) throws alturaInadecuadaException {
        if (altura <(0.5f) || altura > (2.90f)) {
            throw new alturaInadecuadaException(" La altura no es correcta");
        }
        this.altura = altura;
    }

    public String imprime() {
        return this.nombre + ", con DNI " + this.dni + ", que nacio " + this.nacimiento + ", la altura es de " + this.altura + ", su genero es " + this.genero;
    }
}
