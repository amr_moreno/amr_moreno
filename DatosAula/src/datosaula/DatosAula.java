/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datosaula;

import excepciones.DniInvalidoException;
import excepciones.PersonaException;
import excepciones.alturaInadecuadaException;
import excepciones.generoEquibocado;
import excepciones.hasNacidoMAñanaException;
import excepciones.nombreMayorException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.converter.LocalDateTimeStringConverter;

/**
 *
 * @author 1DAM
 */
public class DatosAula {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        File logFile = null;
        FileWriter log = null;
        BufferedReader lectorDatos = null;
        BufferedReader lectorInicio = null;
        LocalDateTime hoy = LocalDateTime.now();
        try {
            logFile = new File("./datosAula.log");
            log = new FileWriter(logFile, true);
            lectorInicio = new BufferedReader(new FileReader(logFile));

            //llamra a log.writer() para escribir en el fichero lo siguiente :
            // [Dia y hora de ejecucuion ] Programa iniciado
            log.write("[" + hoy + "]" + " Programa Iniciado " + "\n");
            log.flush();

            File archivosDatos = new File("./datos.cenec");
            lectorDatos = new BufferedReader(new FileReader(archivosDatos));
            String nombre = null;
            String dni = null;
            LocalDate nacimiento = null;
            Float altura = null;
            String genero = null;
            int i = 0;
            do {
                nombre = lectorDatos.readLine();
                dni = lectorDatos.readLine();
                genero = lectorDatos.readLine();

                if (nombre != null && dni != null) {
                    altura = Float.parseFloat(lectorDatos.readLine());
                    nacimiento = Date.valueOf(lectorDatos.readLine()).toLocalDate();
                    Persona p = new Persona(nombre, dni, nacimiento, altura, genero);
                    System.out.println(p.imprime());
                    i++;
                }

            } while (nombre != null && dni != null);
            //Escribir en el log 
            //[]Programa finalizado. Personas encontradas :X
            log.write("[" + hoy + "]" + " Programa finlizado. Perosnas encontradas: " + i + "\n");
            log.flush();
        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
            try {
                // System.err.println(ex.getMessage());
                // ex.printStackTrace();
                //log.Writer [] Error:Archivo no encontrado
                log.write("[" + hoy + "]" + " Error: Archivo no encontrado"+ "\n");
                log.flush();
            } catch (IOException ex1) {
                Logger.getLogger(DatosAula.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            System.err.println("Algo sale mal en la lectura");

        } catch (PersonaException ex) {
            ex.printStackTrace();
            try {
                log.write("[" + hoy + "]" + "Error: " + ex.getMessage()+ "\n");
                log.flush();
            } catch (IOException ex1) {
                Logger.getLogger(DatosAula.class.getName()).log(Level.SEVERE, null, ex1);
            }

        } finally {
            try {
                lectorDatos.close();
            } catch (IOException ex) {
                Logger.getLogger(DatosAula.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
