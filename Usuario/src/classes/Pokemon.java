/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author 1DAM
 */
public class Pokemon extends Livingbeing {

    private PokemonType type;
    private byte level;
    private short lifePoints;

            
            public enum PokemonType{
        FUEGO,AGUA,PLANTA
    }

    public Pokemon(String name, boolean genre, String description,PokemonType type, byte level, short lifePoints ) {
        super(name, genre, description);
        this.type = type;
        this.level = level;
        this.lifePoints = lifePoints;
    }

    public PokemonType getType() {
        return type;
    }

    public void setType(PokemonType type) {
        this.type = type;
    }

    public byte getLevel() {
        return level;
    }

    public void setLevel(byte level) {
        this.level = level;
    }

    public short getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(short lifePoints) {
        this.lifePoints = lifePoints;
    }
    
}
