
package examen2t_poo;

public class plantaOrnamental extends Planta {
    
    private String nombre;
    private String tipo;
    
    private int tiempoFrescor;
    private int frecuenciaRegado;

    public plantaOrnamental(float precio,byte nombreT,String nombre, String tipo, int tiempoFrescor, int frecuenciaRegado) {
        super(precio,nombreT);
        this.nombre = nombre;
        this.tipo = tipo;
        this.tiempoFrescor = tiempoFrescor;
        this.frecuenciaRegado = frecuenciaRegado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getTiempoFrescor() {
        return tiempoFrescor;
    }

    public void setTiempoFrescor(int tiempoFrescor) {
        this.tiempoFrescor = tiempoFrescor;
    }

    public int getFrecuenciaRegado() {
        return frecuenciaRegado;
    }

    public void setFrecuenciaRegado(int frecuenciaRegado) {
        this.frecuenciaRegado = frecuenciaRegado;
    }
    
}
