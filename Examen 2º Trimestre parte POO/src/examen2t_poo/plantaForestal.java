
package examen2t_poo;

public class plantaForestal extends Planta {
    
    private String nombre;
    private String tipoSuelo;
    private String tipoHoja;
    private String fruto;

    public plantaForestal(int precio,byte nombreT, String nombre, String tipoSuelo, String tipoHoja, String fruto) {
        super(precio,nombreT);
        this.nombre = nombre;
        this.tipoSuelo = tipoSuelo;
        this.tipoHoja = tipoHoja;
        this.fruto = fruto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoSuelo() {
        return tipoSuelo;
    }

    public void setTipoSuelo(String tipoSuelo) {
        this.tipoSuelo = tipoSuelo;
    }

    public String getTipoHoja() {
        return tipoHoja;
    }

    public void setTipoHoja(String tipoHoja) {
        this.tipoHoja = tipoHoja;
    }

    public String getFruto() {
        return fruto;
    }

    public void setFruto(String fruto) {
        this.fruto = fruto;
    }
    
}
