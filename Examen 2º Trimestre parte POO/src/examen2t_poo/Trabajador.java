
package examen2t_poo;

public class Trabajador extends PersonaJuridica{
    
  
    private String dni;
    private float sueldo;

    public Trabajador(String nombre, String dni, String telefono, String direccion, String email,float sueldo) {
        super(nombre, telefono, direccion, email);
        this.dni = dni;
        this.sueldo = sueldo;
    }

    
    public float getSueldo() {
        return sueldo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }
    
}
