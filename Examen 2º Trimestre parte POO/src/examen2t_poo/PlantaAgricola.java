
package examen2t_poo;


public class PlantaAgricola extends Planta {

    private String mesSiembra;
    private int tiempoGerminacion;
    private String especie;
    private float precio;
  
    public PlantaAgricola(int precio,byte nombreT ,String mesSiembra, int tiempoGerminacion, String especie) {
      super(precio,nombreT);
        this.mesSiembra = mesSiembra;
        this.tiempoGerminacion = tiempoGerminacion;
        this.especie = especie;
    }

    public String getMesSiembra() {
        return mesSiembra;
    }

    public void setMesSiembra(String mesSiembra) {
        this.mesSiembra = mesSiembra;
    }

    public int getTiempoGerminacion() {
        return tiempoGerminacion;
    }

    public void setTiempoGerminacion(int tiempoGerminacion) {
        this.tiempoGerminacion = tiempoGerminacion;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
           
        this.especie = especie;
    }
    
   
}
