
package examen2t_poo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Factura {
    
    private LocalDateTime fecha;
    private Planta[]plantasCompradas;
    private Trabajador trabajador;

    /**
     * Constructor de factura
     * @param fecha
     * @param plantasCompradas
     * @param trabajador 
     */
    public Factura(LocalDateTime fecha, Planta[] plantasCompradas, Trabajador trabajador) {
        this.fecha = fecha;
        this.plantasCompradas = plantasCompradas;
        this.trabajador = trabajador;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Planta[] getPlantasCompradas() {
        return plantasCompradas;
    }

    public void setPlantasCompradas(Planta[] plantasCompradas) {
        this.plantasCompradas = plantasCompradas;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
    
}
