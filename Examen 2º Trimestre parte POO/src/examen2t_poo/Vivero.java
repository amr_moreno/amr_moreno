
package examen2t_poo;

import java.time.LocalDateTime;


public class Vivero extends PersonaJuridica{
 
    private Trabajador []trabajadores;
    private Planta [] stock;
    private int cantidadDinero;
    private Factura[]facturas;

    public Vivero(String nombre, String direccion, String telefono, String email, Trabajador[] trabajadores, Planta[] plantas, int cantidadDinero, Factura[] facturas) {
      super(nombre, telefono, direccion, email); 
        this.trabajadores = trabajadores;
        this.stock = plantas;
        this.cantidadDinero = cantidadDinero;
        this.facturas = new Factura[10000];
    }


    public Trabajador[] getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(Trabajador[] trabajadores) {
        this.trabajadores = trabajadores;
    }

    public Planta[] getPlantas() {
        return stock;
    }

    public void setPlantas(Planta[] plantas) {
        this.stock= plantas;
    }

    public int getCantidadDinero() {
        return cantidadDinero;
    }

    public void setCantidadDinero(int cantidadDinero) {
        this.cantidadDinero = cantidadDinero;
    }

    public Factura[] getFacturas() {
        return facturas;
    }

    public void setFacturas(Factura[] facturas) {
        this.facturas = facturas;
    }
    public Factura vender (Planta [] p,Trabajador t) {
        Factura f=new Factura(LocalDateTime.now(), p, t);
        int primeraNula =0;
        for (int i = 0; i < facturas.length; i++) {
            if ( facturas[i]==null) {
               primeraNula=i;
               break;
                
            }
            
        }
        
       facturas[primeraNula]=f;
        for (int i = 0; i <p.length ; i++) {
            this.cantidadDinero(this.getCantidadDinero()+p[i].getPrecio());
        }
  return f;}

    
}
